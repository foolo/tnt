package undo_manager;

public interface UndoableModel {

	public UndoableModel copy();
}
