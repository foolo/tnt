package xliff_model.exceptions;

public class XliffVersionException extends ParseException {

	public XliffVersionException(String msg) {
		super(msg);
	}
}
