package xliff_model.exceptions;

public class SaveException extends Exception {

	public SaveException(String msg) {
		super(msg);
	}
}
