package xliff_model;

public interface TaggedTextContent {

	TaggedTextContent copy();
}
